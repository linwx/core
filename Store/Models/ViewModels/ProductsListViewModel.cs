using System.Collections.Generic; 
using Store.Models; 
namespace Store.Models.ViewModels { 
    public class ProductsListViewModel { 
        public IEnumerable<Product> Products { get; set; } 
        public PageInfo PagingInfo { get; set; } 
    } 
} 