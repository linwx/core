using Microsoft.EntityFrameworkCore;

namespace Store.Models { 
       public class ApplicationDbContext : DbContext { 
           public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) 
             : base(options) {} 
           public DbSet<Product> Products { get; set; } 
           public DbSet<Category> Categories { get; set; } 

           public DbSet<Account> Accounts { get; set; } 

     } 
 } 