﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Store.Models;
using Store.Models.ViewModels;

namespace Store.Controllers
{
    public class HomeController : Controller
    {
        private IProductRepository repository;
        public int pageSize = 4;
        public HomeController(IProductRepository repo)
        {
            repository = repo;
        }
        public ViewResult List(int page = 1)
        {
            return View(new ProductsListViewModel{
               Products = repository.Products.OrderBy(x=>x.ID).Skip((page-1)*pageSize).Take(pageSize),
               PagingInfo = new PageInfo{
                   CurrentPage = page,
                   ItemsPerPage = pageSize,
                   TotalItems = repository.Products.Count()
               }
            });
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
