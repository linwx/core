using System.Collections.Generic; 
using System.Threading.Tasks; 
using Microsoft.AspNetCore.Mvc; 
using Microsoft.AspNetCore.Mvc.Routing; 
using Microsoft.AspNetCore.Razor.TagHelpers; 
using Moq; 
using Store.Models;
using Store.Models.ViewModels; 
using Xunit; 
using Store.Controllers;

namespace Store.Tests
{
    public class PageViewModelTest
    {
        [Fact] 
        public void Can_Send_Pagination_View_Model() { 
        // Arrange 
        Mock<IProductRepository> mock = new Mock<IProductRepository>(); 
        mock.Setup(m => m.Products).Returns(new Product[] { 
            new Product {ID = 1, Name = "P1"}, 
                new Product {ID = 2, Name = "P2"}, 
                new Product {ID = 3, Name = "P3"}, 
                new Product {ID = 4, Name = "P4"}, 
                new Product {ID = 5, Name = "P5"} 
            }); 
            // Arrange 
            var controller = 
                new HomeController(mock.Object) { pageSize = 3 }; 
            // Act 
            ProductsListViewModel result = 
                controller.List(2).ViewData.Model as ProductsListViewModel; 
            // Assert 
            PageInfo pageInfo = result.PagingInfo; 
            Assert.Equal(2, pageInfo.CurrentPage); 
            Assert.Equal(3, pageInfo.ItemsPerPage); 
            Assert.Equal(5, pageInfo.TotalItems); 
            Assert.Equal(2, pageInfo.TotalPages); 
        } 
    }
    
}