using System;
using System.Collections.Generic; 
using System.Linq; 
using Xunit;
using Moq;
using Store.Models;
using Store.Models.ViewModels;
using Store.Controllers;

namespace Store.Tests
{
    public class UnitTest1
    {
        [Fact] 
        public void Can_Paginate() { 
            // Arrange 
            Mock<IProductRepository> mock = new Mock<IProductRepository>(); 
            mock.Setup(m => m.Products).Returns(new Product[] { 
                new Product {ID = 1, Name = "P1"}, 
                new Product {ID = 2, Name = "P2"}, 
                new Product {ID = 3, Name = "P3"}, 
                new Product {ID = 4, Name = "P4"}, 
                new Product {ID = 5, Name = "P5"} 
            }); 
            var controller = new HomeController(mock.Object); 
            controller.pageSize = 3; 
                // Act 
            ProductsListViewModel result = 
                controller.List(2).ViewData.Model as ProductsListViewModel; 
                // Assert 
            Product[] prodArray = result.Products.ToArray(); 
            Assert.True(prodArray.Length == 2); 
            Assert.Equal("P4", prodArray[0].Name); 
            Assert.Equal("P5", prodArray[1].Name); 
        } 
    }
}
